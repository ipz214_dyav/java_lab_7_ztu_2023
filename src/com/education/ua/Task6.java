package com.education.ua;

import java.util.Scanner;

public class Task6 {
    private static String data;

    public static void main(String[] args) {
        Object lock = new Object();


        Thread readerThread = new Thread(() -> {
            while (true) {
                synchronized (lock) {
                    try {
                        System.out.println("Reader ����� �������� �����...");
                        lock.wait();
                        System.out.println("Reader ��������: " + data);
                        Thread.sleep(1000);
                        System.out.println("Reader �������� ������� �����.");
                        lock.notify();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        Thread printerThread = new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while (true) {
                synchronized (lock) {
                    System.out.println("������ ����:");
                    data = scanner.nextLine();
                    lock.notify();
                    try {
                        System.out.println("Printer ����� ���������� ������� ����� � Reader...");
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        readerThread.start();
        printerThread.start();
    }
}
