package com.education.ua;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Task7MultiThreads {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        long start = System.currentTimeMillis();

        for (int i = 0; i < 5; i++) {
            Runnable task = new SumOfDigitsInArray();
            executorService.execute(task);
        }

        executorService.shutdown();

        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("��������� ��� ��������� ��� ������: " + (end - start) + " ��");

    }
}
