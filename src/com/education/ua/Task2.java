package com.education.ua;

public class Task2 {

    public static void main(String[] args){
        MyThread myThread = new MyThread();
        System.out.println("���� ������ ���� ���������: " + myThread.getState()); // ���� NEW
        myThread.start();
        System.out.println("���� ������ ���� �������: " + myThread.getState());

        System.out.println("��'� ������: " + myThread.getName());
        System.out.println("������� ������: " + myThread.getPriority());
        System.out.println("��������������� ����: " + myThread.isAlive());
        System.out.println("���� �����: " + myThread.isDaemon());

        myThread.setName("lab7");
        myThread.setPriority(2);
        System.out.println("���� ��'� ������: " + myThread.getName());
        System.out.println("����� ������� ������: " + myThread.getPriority());

        try {
            myThread.join();
            System.out.println("���� �������� ������.");
            System.out.println("���� ������ ���� ����������: " + myThread.getState());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("��'� ��������� ������: " + Thread.currentThread().getName());
        System.out.println("������� ��������� ������: " + Thread.currentThread().getPriority());

    }
}
