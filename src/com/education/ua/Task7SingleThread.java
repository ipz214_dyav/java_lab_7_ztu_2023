package com.education.ua;

public class Task7SingleThread {
    public static void main(String[] args) {
        SumOfDigitsInArray sumOfDigitsInArray = new SumOfDigitsInArray();
        Thread thread1 = new Thread(sumOfDigitsInArray);

        long start = System.currentTimeMillis();
        thread1.start();
        try {
            thread1.join();
            long end = System.currentTimeMillis();
            System.out.println("��� ��������� ����: " + (end - start) + "��");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
